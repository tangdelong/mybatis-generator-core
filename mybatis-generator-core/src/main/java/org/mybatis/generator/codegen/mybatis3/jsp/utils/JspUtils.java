package org.mybatis.generator.codegen.mybatis3.jsp.utils;

/**
 * JSP工具类 
 *
 * @author tangdelong
 * 2015年7月8日
 */
public class JspUtils {
	
	
	/**
	 * JSP 引入文件1
	 *
	 * @author tangdelong
	 * 2015年7月8日
	 */
	public static String getInclude1(){
		StringBuilder sb = new StringBuilder();
		sb.append("<%@ include file=\"/includes/taglibs.jsp\" %>");
		return sb.toString();
	}
	
	/**
	 * JSP 引入文件2
	 *
	 * @author tangdelong
	 * 2015年7月8日
	 */
	public static String getInclude2(){
		StringBuilder sb = new StringBuilder();
		sb.append("\n<%@ include file=\"/includes/common_js.jsp\" %>");
		sb.append("\n<%@ include file=\"/includes/common_css.jsp\" %>");
		return sb.toString();
	}
	
}
