/*
 *  Copyright 2009 The Apache Software Foundation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.mybatis.generator.codegen.mybatis3;

import java.util.ArrayList;
import java.util.List;

import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.GeneratedJspFile;
import org.mybatis.generator.api.GeneratedXmlFile;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.ProgressCallback;
import org.mybatis.generator.api.dom.java.CompilationUnit;
import org.mybatis.generator.api.dom.jsp.JspDocument;
import org.mybatis.generator.api.dom.xml.Document;
import org.mybatis.generator.codegen.AbstractGenerator;
import org.mybatis.generator.codegen.AbstractJavaClientGenerator;
import org.mybatis.generator.codegen.AbstractJavaGenerator;
import org.mybatis.generator.codegen.AbstractXmlGenerator;
import org.mybatis.generator.codegen.mybatis3.controller.JavaControllerGenerator;
import org.mybatis.generator.codegen.mybatis3.javamapper.AnnotatedClientGenerator;
import org.mybatis.generator.codegen.mybatis3.javamapper.JavaMapperGenerator;
import org.mybatis.generator.codegen.mybatis3.javamapper.MixedClientGenerator;
import org.mybatis.generator.codegen.mybatis3.jsp.JspGeneratorFactory;
import org.mybatis.generator.codegen.mybatis3.model.BaseRecordConditionGenerator;
import org.mybatis.generator.codegen.mybatis3.model.BaseRecordGenerator;
import org.mybatis.generator.codegen.mybatis3.model.ExampleGenerator;
import org.mybatis.generator.codegen.mybatis3.model.PrimaryKeyGenerator;
import org.mybatis.generator.codegen.mybatis3.model.RecordWithBLOBsGenerator;
import org.mybatis.generator.codegen.mybatis3.service.JavaServiceGenerator;
import org.mybatis.generator.codegen.mybatis3.xmlmapper.XMLMapperGenerator;
import org.mybatis.generator.config.PropertyRegistry;
import org.mybatis.generator.internal.ObjectFactory;

/**
 * 
 * @author Jeff Butler
 * 
 */
public class IntrospectedTableMyBatis3Impl extends IntrospectedTable {
    protected List<AbstractJavaGenerator> javaModelGenerators;
    protected List<AbstractJavaGenerator> javaModelConditionGenerators;
    protected List<AbstractJavaGenerator> clientGenerators;
    protected AbstractXmlGenerator xmlMapperGenerator;
    protected List<AbstractJavaGenerator> serviceGenerators;
    protected List<AbstractJavaGenerator> controllerGennerators;
    
    protected JspGeneratorFactory jspGeneratorFactory;
    

    public IntrospectedTableMyBatis3Impl() {
        super(TargetRuntime.MYBATIS3);
        javaModelGenerators = new ArrayList<AbstractJavaGenerator>();
        javaModelConditionGenerators = new ArrayList<AbstractJavaGenerator>();
        clientGenerators = new ArrayList<AbstractJavaGenerator>();
        serviceGenerators = new ArrayList<AbstractJavaGenerator>();
        controllerGennerators = new ArrayList<AbstractJavaGenerator>();
    }

    @Override
    public void calculateGenerators(List<String> warnings,
            ProgressCallback progressCallback) {
        calculateJavaModelGenerators(warnings, progressCallback);
        
        // 新增：ModeCondition
        calculateJavaModelConditionGenerators(warnings, progressCallback);
        
        // 新增：service
        calculateServiceGenerators(warnings, progressCallback);
        
        // 新增： controller
        calculateControllerGenerators(warnings, progressCallback);
        
        AbstractJavaClientGenerator javaClientGenerator =
            calculateClientGenerators(warnings, progressCallback);
            
        calculateXmlMapperGenerator(javaClientGenerator, warnings, progressCallback);
        
        // 新增 ： jspList页
        calculateJspListGenerator(warnings, progressCallback);
    }

    protected void calculateXmlMapperGenerator(AbstractJavaClientGenerator javaClientGenerator, 
            List<String> warnings,
            ProgressCallback progressCallback) {
        if (javaClientGenerator == null) {
            if (context.getSqlMapGeneratorConfiguration() != null) {
                xmlMapperGenerator = new XMLMapperGenerator();
            }
        } else {
            xmlMapperGenerator = javaClientGenerator.getMatchedXMLGenerator();
        }
        
        initializeAbstractGenerator(xmlMapperGenerator, warnings,
                progressCallback);
    }
    
    /**
     * 计算JspList页
     *
     * @author tangdelong
     * 2015年6月17日
     */
    protected void calculateJspListGenerator(List<String> warnings,ProgressCallback progressCallback) {
    	
    	jspGeneratorFactory = new JspGeneratorFactory();
    	
    	initializeAbstractGenerator(jspGeneratorFactory, warnings,
    			progressCallback);
    }

    /**
     * 
     * @param warnings
     * @param progressCallback
     * @return true if an XML generator is required
     */
    protected AbstractJavaClientGenerator calculateClientGenerators(List<String> warnings,
            ProgressCallback progressCallback) {
        if (!rules.generateJavaClient()) {
            return null;
        }
        
        AbstractJavaClientGenerator javaGenerator = createJavaClientGenerator();
        if (javaGenerator == null) {
            return null;
        }

        initializeAbstractGenerator(javaGenerator, warnings, progressCallback);
        clientGenerators.add(javaGenerator);
        
        return javaGenerator;
    }
    
    
    /**
     * 计算serviceGenerators
     *
     * @author tangdelong
     * 2015年5月22日
     */
    protected AbstractJavaClientGenerator calculateServiceGenerators(List<String> warnings,
            ProgressCallback progressCallback) {
        if (!rules.generateJavaClient()) {
            return null;
        }
        
        AbstractJavaClientGenerator javaGenerator = new JavaServiceGenerator();
        initializeAbstractGenerator(javaGenerator, warnings,
                progressCallback);
        serviceGenerators.add(javaGenerator);
        
        return javaGenerator;
    }
    
    
    
    /**
     * 计算controller
     *
     * @author tangdelong
     * 2015年5月22日
     */
    protected AbstractJavaClientGenerator calculateControllerGenerators(List<String> warnings,
            ProgressCallback progressCallback) {
        if (!rules.generateJavaClient()) {
            return null;
        }
        
        AbstractJavaClientGenerator javaGenerator = new JavaControllerGenerator();
        initializeAbstractGenerator(javaGenerator, warnings,
                progressCallback);
        this.controllerGennerators.add(javaGenerator);
        
        return javaGenerator;
    }
    
    
    protected AbstractJavaClientGenerator createJavaClientGenerator() {
        if (context.getJavaClientGeneratorConfiguration() == null) {
            return null;
        }
        
        String type = context.getJavaClientGeneratorConfiguration()
                .getConfigurationType();

        AbstractJavaClientGenerator javaGenerator;
        if ("XMLMAPPER".equalsIgnoreCase(type)) { //$NON-NLS-1$
            javaGenerator = new JavaMapperGenerator();
        } else if ("MIXEDMAPPER".equalsIgnoreCase(type)) { //$NON-NLS-1$
            javaGenerator = new MixedClientGenerator();
        } else if ("ANNOTATEDMAPPER".equalsIgnoreCase(type)) { //$NON-NLS-1$
            javaGenerator = new AnnotatedClientGenerator();
        } else if ("MAPPER".equalsIgnoreCase(type)) { //$NON-NLS-1$
            javaGenerator = new JavaMapperGenerator();
        } else {
            javaGenerator = (AbstractJavaClientGenerator) ObjectFactory
                    .createInternalObject(type);
        }
        
        return javaGenerator;
    }

    protected void calculateJavaModelGenerators(List<String> warnings,
            ProgressCallback progressCallback) {
        if (getRules().generateExampleClass()) {
            AbstractJavaGenerator javaGenerator = new ExampleGenerator();
            initializeAbstractGenerator(javaGenerator, warnings,
                    progressCallback);
            javaModelGenerators.add(javaGenerator);
        }

        if (getRules().generatePrimaryKeyClass()) {
            AbstractJavaGenerator javaGenerator = new PrimaryKeyGenerator();
            initializeAbstractGenerator(javaGenerator, warnings,
                    progressCallback);
            javaModelGenerators.add(javaGenerator);
        }

        if (getRules().generateBaseRecordClass()) {
            AbstractJavaGenerator javaGenerator = new BaseRecordGenerator();
            initializeAbstractGenerator(javaGenerator, warnings,
                    progressCallback);
            javaModelGenerators.add(javaGenerator);
        }

        if (getRules().generateRecordWithBLOBsClass()) {
            AbstractJavaGenerator javaGenerator = new RecordWithBLOBsGenerator();
            initializeAbstractGenerator(javaGenerator, warnings,
                    progressCallback);
            javaModelGenerators.add(javaGenerator);
        }
    }
    
    
    /**
     * 新增JavaModeConditionGenerators
     *
     * @author tangdelong
     * 2015年5月21日
     */
    protected void calculateJavaModelConditionGenerators(List<String> warnings,
            ProgressCallback progressCallback) {

    	if(this.context.getJavaModeConditionGeneratorConfiguration() != null){
	        if (getRules().generateBaseRecordClass()) {
	            AbstractJavaGenerator javaGenerator = new BaseRecordConditionGenerator();
	            initializeAbstractGenerator(javaGenerator, warnings,progressCallback);
	            javaModelConditionGenerators.add(javaGenerator);
	        }
    	}

    }
    

    protected void initializeAbstractGenerator(
            AbstractGenerator abstractGenerator, List<String> warnings,
            ProgressCallback progressCallback) {
        if (abstractGenerator == null) {
            return;
        }
        
        abstractGenerator.setContext(context);
        abstractGenerator.setIntrospectedTable(this);
        abstractGenerator.setProgressCallback(progressCallback);
        abstractGenerator.setWarnings(warnings);
    }

    @Override
    public List<GeneratedJavaFile> getGeneratedJavaFiles() {
        List<GeneratedJavaFile> answer = new ArrayList<GeneratedJavaFile>();

        for (AbstractJavaGenerator javaGenerator : javaModelGenerators) {
            List<CompilationUnit> compilationUnits = javaGenerator
                    .getCompilationUnits();
            for (CompilationUnit compilationUnit : compilationUnits) {
                GeneratedJavaFile gjf = new GeneratedJavaFile(compilationUnit,
                        context.getJavaModelGeneratorConfiguration()
                                .getTargetProject(),
                                context.getProperty(PropertyRegistry.CONTEXT_JAVA_FILE_ENCODING),
                                context.getJavaFormatter());
                answer.add(gjf);
            }
        }
        
        // 新增ModeCondition
        for (AbstractJavaGenerator javaGenerator : javaModelConditionGenerators) {
        	List<CompilationUnit> compilationUnits = javaGenerator
        			.getCompilationUnits();
        	for (CompilationUnit compilationUnit : compilationUnits) {
        		GeneratedJavaFile gjf = new GeneratedJavaFile(compilationUnit,
        				context.getJavaModeConditionGeneratorConfiguration()
        				.getTargetProject(),
        				context.getProperty(PropertyRegistry.CONTEXT_JAVA_FILE_ENCODING),
        				context.getJavaFormatter());
        		answer.add(gjf);
        	}
        }

        for (AbstractJavaGenerator javaGenerator : clientGenerators) {
            List<CompilationUnit> compilationUnits = javaGenerator
                    .getCompilationUnits();
            for (CompilationUnit compilationUnit : compilationUnits) {
                GeneratedJavaFile gjf = new GeneratedJavaFile(compilationUnit,
                        context.getJavaClientGeneratorConfiguration()
                                .getTargetProject(),
                                context.getProperty(PropertyRegistry.CONTEXT_JAVA_FILE_ENCODING),
                                context.getJavaFormatter());
                answer.add(gjf);
            }
        }
        
        // 新增service
        if(context.getJavaServiceGeneratorConfiguration() != null){
	        for (AbstractJavaGenerator javaGenerator : serviceGenerators) {
	            List<CompilationUnit> compilationUnits = javaGenerator
	                    .getCompilationUnits();
	            for (CompilationUnit compilationUnit : compilationUnits) {
	                GeneratedJavaFile gjf = new GeneratedJavaFile(compilationUnit,
	                        context.getJavaServiceGeneratorConfiguration().getTargetProject(),
	                                context.getProperty(PropertyRegistry.CONTEXT_JAVA_FILE_ENCODING),
	                                context.getJavaFormatter());
	                answer.add(gjf);
	            }
	        }
        }
        
        
        // 新增controller
        if(context.getJavaControllerGeneratorConfiguration() != null){
	        for (AbstractJavaGenerator javaGenerator : controllerGennerators) {
	        	List<CompilationUnit> compilationUnits = javaGenerator
	        			.getCompilationUnits();
	        	for (CompilationUnit compilationUnit : compilationUnits) {
	        		GeneratedJavaFile gjf = new GeneratedJavaFile(compilationUnit,
	        				context.getJavaControllerGeneratorConfiguration().getTargetProject(),
	        				context.getProperty(PropertyRegistry.CONTEXT_JAVA_FILE_ENCODING),
	        				context.getJavaFormatter());
	        		answer.add(gjf);
	        	}
	        }
        }


        return answer;
    }

    @Override
    public List<GeneratedXmlFile> getGeneratedXmlFiles() {
        List<GeneratedXmlFile> answer = new ArrayList<GeneratedXmlFile>();

        if (xmlMapperGenerator != null) {
            Document document = xmlMapperGenerator.getDocument();
            GeneratedXmlFile gxf = new GeneratedXmlFile(document,
                getMyBatis3XmlMapperFileName(), getMyBatis3XmlMapperPackage(),
                context.getSqlMapGeneratorConfiguration().getTargetProject(),
                false, context.getXmlFormatter());
            if (context.getPlugins().sqlMapGenerated(gxf, this)) {
                answer.add(gxf);
            }
        }

        return answer;
    }
    
    /**
     * 新增：JSP生成
     *
     * @author tangdelong
     * 2015年6月18日
     */
    @Override
    public List<GeneratedJspFile> getGeneratedJspFiles() {
        List<GeneratedJspFile> answer = new ArrayList<GeneratedJspFile>();

        if (jspGeneratorFactory != null && context.getJavaJSPGeneratorConfiguration() != null) {
        	
        	// JSP 列表显示
            JspDocument document = jspGeneratorFactory.getJspGenerator("list").getJspDocument();
            GeneratedJspFile gxf = new GeneratedJspFile(document,
                getMyBatis3JSPListFileName(), 
                context.getJavaJSPGeneratorConfiguration().getTargetPackage()+"\\"+fullyQualifiedTable.getDomainObjectName().toLowerCase(),
                context.getJavaJSPGeneratorConfiguration().getTargetProject());
            
                answer.add(gxf);
                
            // JSP 添加
            JspDocument document1 = jspGeneratorFactory.getJspGenerator("add").getJspDocument();
            GeneratedJspFile gxf1 = new GeneratedJspFile(document1,
                getMyBatis3JSPAddFileName(), 
                context.getJavaJSPGeneratorConfiguration().getTargetPackage()+"\\"+fullyQualifiedTable.getDomainObjectName().toLowerCase(),
                context.getJavaJSPGeneratorConfiguration().getTargetProject());
            
                answer.add(gxf1);
            // JSP 详情  
	        JspDocument document2 = jspGeneratorFactory.getJspGenerator("detail").getJspDocument();
	        GeneratedJspFile gxf2 = new GeneratedJspFile(document2,
	            getMyBatis3JSPDetailFileName(), 
	            context.getJavaJSPGeneratorConfiguration().getTargetPackage()+"\\"+fullyQualifiedTable.getDomainObjectName().toLowerCase(),
	            context.getJavaJSPGeneratorConfiguration().getTargetProject());
	        
	            answer.add(gxf2);
	            
            // JSP 修改
	        JspDocument document3 = jspGeneratorFactory.getJspGenerator("detail").getJspDocument();
	        GeneratedJspFile gxf3 = new GeneratedJspFile(document3,
	            getMyBatis3JSPUpdateFileName(), 
	            context.getJavaJSPGeneratorConfiguration().getTargetPackage()+"\\"+fullyQualifiedTable.getDomainObjectName().toLowerCase(),
	            context.getJavaJSPGeneratorConfiguration().getTargetProject());
	        
	            answer.add(gxf3);

        }

        return answer;
    }

    @Override
    public int getGenerationSteps() {
        return javaModelGenerators.size() + clientGenerators.size() +
            (xmlMapperGenerator == null ? 0 : 1);
    }

    @Override
    public boolean isJava5Targeted() {
        return true;
    }

    @Override
    public boolean requiresXMLGenerator() {
        AbstractJavaClientGenerator javaClientGenerator =
            createJavaClientGenerator();
        
        if (javaClientGenerator == null) {
            return false;
        } else {
            return javaClientGenerator.requiresXMLGenerator();
        }
    }
}
