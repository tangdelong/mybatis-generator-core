package org.mybatis.generator.codegen.mybatis3.xmlmapper.elements;


import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.codegen.mybatis3.MyBatis3FormattingUtilities;

/**
 * 生成等于查询xml
 * @author tangdelong
 * 2015年12月1日
 */
public class EqualColumnElementGenerator extends AbstractXmlElementGenerator {

	@Override
	public void addElements(XmlElement parentElement) {
		XmlElement answer = new XmlElement("sql"); 

        answer.addAttribute(new Attribute("id", "equal")); 
        
        context.getCommentGenerator().addComment(answer);
        

        StringBuilder sb = new StringBuilder();
        
        for (IntrospectedColumn introspectedColumn : introspectedTable.getAllColumns()) {
        	 XmlElement insertNotNullElement = new XmlElement("if"); //$NON-NLS-1$
             sb.setLength(0);
             sb.append(introspectedColumn.getJavaProperty());
             sb.append(" != null"); //$NON-NLS-1$
             
             boolean flag = false;
             // 特殊类型处理
             if("java.lang.String".equals(introspectedColumn.getFullyQualifiedJavaType().toString())){
            	 flag = true;
             }
             
             if(flag){
             	sb.append(" and ");
 	           	sb.append(introspectedColumn.getJavaProperty());
 	           	sb.append(" != '' ");
             }
             
             insertNotNullElement.addAttribute(new Attribute("test", sb.toString()));
             
             
             sb.setLength(0);
             // 判断字段类型
             String columnType = introspectedColumn.getFullyQualifiedJavaType().toString();
             
             if(columnType.equals("java.sql.Timestamp") || columnType.equals("java.util.Date")){ // 时间类型处理
            	 sb.append(" <![CDATA[");
            	 sb.append(" AND ");
                 sb.append(MyBatis3FormattingUtilities
                         .getEscapedColumnName(introspectedColumn));
                 sb.append(" = "); //$NON-NLS-1$
                 sb.append("#{"+introspectedColumn.getJavaProperty(null)+"}");
                 sb.append(" ]]>");
             }else{
            	 sb.append(" AND ");
                 sb.append(MyBatis3FormattingUtilities
                         .getEscapedColumnName(introspectedColumn));
                 sb.append(" = "); //$NON-NLS-1$
                 sb.append("#{"+introspectedColumn.getJavaProperty(null)+"}");
             }
             
             insertNotNullElement.addElement(new TextElement(sb.toString()));
             
             answer.addElement(insertNotNullElement);

        }

        if (context.getPlugins().sqlMapSelectByPrimaryKeyElementGenerated(answer,introspectedTable)) {
            parentElement.addElement(answer);
        }

	}

}
