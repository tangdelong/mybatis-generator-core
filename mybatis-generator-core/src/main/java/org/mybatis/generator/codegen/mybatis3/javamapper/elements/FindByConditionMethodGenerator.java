package org.mybatis.generator.codegen.mybatis3.javamapper.elements;

import java.util.Set;
import java.util.TreeSet;

import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.JavaVisibility;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.Parameter;

/**
 * 新增：根据条件查询方法
 *
 * @author tangdelong
 * 2015年5月15日
 */
public class FindByConditionMethodGenerator extends AbstractJavaMapperMethodGenerator {

	@Override
	public void addInterfaceElements(Interface interfaze) {
		Set<FullyQualifiedJavaType> importedTypes = new TreeSet<FullyQualifiedJavaType>();
        Method method = new Method();

        // 定义返回类型为List
        FullyQualifiedJavaType returnListType = new FullyQualifiedJavaType("java.util.List");
        FullyQualifiedJavaType returnType = introspectedTable.getRules()
                .calculateAllFieldsClass();
        
        returnListType.addTypeArgument(returnType);
        
        method.setReturnType(returnListType);
        
        importedTypes.add(returnType);
        importedTypes.add(returnListType);
        
        
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName(introspectedTable.getLikeStatementId());

        method.addParameter(new Parameter(returnType, "condition")); //$NON-NLS-1$

        context.getCommentGenerator().addGeneralMethodComment(method,
                introspectedTable,"根据查询条件进行模糊查询");

        
        if (context.getPlugins().clientInsertSelectiveMethodGenerated(
                method, interfaze, introspectedTable)) {
            interfaze.addImportedTypes(importedTypes);
            interfaze.addMethod(method);
        }
		
	}

}
