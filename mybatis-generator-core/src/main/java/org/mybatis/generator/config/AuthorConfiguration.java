package org.mybatis.generator.config;

/**
 * 作者
 * @author tangdelong
 * 2015年12月1日
 */
public class AuthorConfiguration {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
