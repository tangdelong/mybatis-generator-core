package org.mybatis.generator.config;

/**
 * 实体类扩展类
 *
 * @author tangdelong
 * 2015年5月14日
 */
public class JavaModelConditionGeneratorConfiguration extends PropertyHolder{
	private String targetPackage;

    private String targetProject;
    
    /**
     * 后缀名称
     */
    private String suffixName;

	public String getTargetPackage() {
		return targetPackage;
	}

	public void setTargetPackage(String targetPackage) {
		this.targetPackage = targetPackage;
	}

	public String getTargetProject() {
		return targetProject;
	}

	public void setTargetProject(String targetProject) {
		this.targetProject = targetProject;
	}

	public String getSuffixName() {
		return suffixName;
	}

	public void setSuffixName(String suffixName) {
		this.suffixName = suffixName;
	}
}
