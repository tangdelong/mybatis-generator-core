package org.mybatis.generator.config;

/**
 * 生成包结构名称
 * @author tangdelong
 * 2015年12月1日
 */
public class JavaTargetPackageGeneratorConfiguration {

	private String targetPackage;

	public String getTargetPackage() {
		return targetPackage;
	}

	public void setTargetPackage(String targetPackage) {
		this.targetPackage = targetPackage;
	}
	
}
