package org.mybatis.generator.config;

/**
 * 生成文件路径
 *
 * @author tangdelong
 * 2015年5月14日
 */
public class JavaGeneratorLocalPathConfiguration extends PropertyHolder {

	
	/**
	 * 目标路径
	 */
	private String targetProject;

	public String getTargetProject() {
		return targetProject;
	}

	public void setTargetProject(String targetProject) {
		this.targetProject = targetProject;
	}
}
