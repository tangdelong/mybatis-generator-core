package org.mybatis.generator.api.dom;

import org.mybatis.generator.api.JspFormatter;
import org.mybatis.generator.api.dom.jsp.JspDocument;
import org.mybatis.generator.config.Context;

public class DefaultJspFormatter implements JspFormatter{
	protected Context context;

	public void setContext(Context context) {
		this.context = context;
		
	}

	public String getFormattedContent(JspDocument jspDocument) {
		return jspDocument.getFormattedContent();
	}

}
