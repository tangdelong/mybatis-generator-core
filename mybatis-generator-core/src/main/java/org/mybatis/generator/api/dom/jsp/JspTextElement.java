package org.mybatis.generator.api.dom.jsp;

import org.mybatis.generator.api.dom.OutputUtilities;
import org.mybatis.generator.api.dom.xml.Element;

/**
 * jsp元素内容
 *
 * @author tangdelong
 * 2015年6月17日
 */
public class JspTextElement extends Element {
	private String content;
	
	
	public JspTextElement(){
		
	}
	
	public JspTextElement(String content){
		this.content = content;
	}
	
	@Override
    public String getFormattedContent(int indentLevel) {
        StringBuilder sb = new StringBuilder();
        OutputUtilities.jspIndent(sb, indentLevel);
        sb.append(content);
        return sb.toString();
    }

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
