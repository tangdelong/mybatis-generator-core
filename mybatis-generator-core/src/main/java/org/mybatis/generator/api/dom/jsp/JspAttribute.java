package org.mybatis.generator.api.dom.jsp;

/**
 * jsp属性
 *
 * @author tangdelong
 * 2015年6月17日
 */
public class JspAttribute {
	
	private String name;
	private String value;
	
	public JspAttribute(){
		super();
	};
	
	public JspAttribute(String name,String value){
		this.name = name;
		this.value = value;
	}
	
	
	 public String getFormattedContent() {
        StringBuilder sb = new StringBuilder();
        sb.append(name);
        sb.append("=\""); //$NON-NLS-1$
        sb.append(value);
        sb.append('\"');

        return sb.toString();
    }
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
