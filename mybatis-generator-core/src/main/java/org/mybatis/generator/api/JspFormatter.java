package org.mybatis.generator.api;

import org.mybatis.generator.api.dom.jsp.JspDocument;
import org.mybatis.generator.config.Context;

public interface JspFormatter{
	
	void setContext(Context context);
	
    String getFormattedContent(JspDocument jspDocument);

}
