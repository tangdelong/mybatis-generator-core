package org.mybatis.generator.api.dom.jsp;

import java.util.List;

import org.mybatis.generator.api.dom.OutputUtilities;
import org.mybatis.generator.internal.util.CollectionUtils;

public class JspDocument {
	private List<String> includes;

	private JspElement rootElement;

	public JspDocument() {

	}

	public JspDocument(List<String> includes) {
		this.includes = includes;
	}

	public String getFormattedContent() {
		StringBuilder sb = new StringBuilder();

		sb.append("<%@ page language=\"java\" contentType=\"text/html; charset=UTF-8\" pageEncoding=\"UTF-8\"%>"); //$NON-NLS-1$

		if (CollectionUtils.isNotEmpty(includes)) {
			for(String str : includes){
				OutputUtilities.newLine(sb);
				sb.append(str); 
			}
		}

		OutputUtilities.newLine(sb);
		sb.append(rootElement.getFormattedContent(0));

		return sb.toString();
	}


	public JspElement getRootElement() {
		return rootElement;
	}

	public void setRootElement(JspElement rootElement) {
		this.rootElement = rootElement;
	}

	public List<String> getIncludes() {
		return includes;
	}

	public void setIncludes(List<String> includes) {
		this.includes = includes;
	}
}
